package com.example.wahab.mytouchpoint.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wahab.mytouchpoint.R;
import com.example.wahab.mytouchpoint.models.ContactListModel;

import java.util.ArrayList;
import java.util.List;

import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.MyViewHolder> implements SectionIndexer {

    private List<ContactListModel> List_all_user = new ArrayList<>();
    private List<String> mDataArray = new ArrayList<>();
    private ArrayList<Integer> mSectionPositions;
    IndexFastScrollRecyclerView mRecyclerView;
    boolean isSelect = true;


    public ContactListAdapter(List<ContactListModel> list_view) {
        List_all_user = list_view;
    }

    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>();
        mSectionPositions = new ArrayList<>(26);
        for (int i = 0, size = List_all_user.size(); i < size; i++) {
            String section = String.valueOf(List_all_user.get(i).getUserName().charAt(0)).toUpperCase();
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        System.out.println("sssss"+mSectionPositions);
        System.out.println("sssdddss"+sections);
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mSectionPositions.get(sectionIndex);
    }

    @Override
    public ContactListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_list, parent, false);

        return new ContactListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ContactListAdapter.MyViewHolder holder, final int position) {
        final ContactListModel frag = List_all_user.get(position);

        holder.userName.setText(frag.getUserName());

        holder.select_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.select_contact.getTag().equals("unselect")){
                    holder.select_contact.setBackgroundResource(R.drawable.bg_contact);
                    holder.btn_slct.setImageResource(R.drawable.slct);
                    holder.userName.setTextColor(Color.parseColor("#ffffff"));
                    holder.select_contact.setTag("select");
                    holder.select_contact.setPadding(15,15,15,15);
                }else {
                    holder.select_contact.setTag("unselect");
                    holder.select_contact.setBackgroundResource(R.color.white);
                    holder.btn_slct.setImageResource(R.drawable.unslct);
                    holder.userName.setTextColor(Color.parseColor("#a3a5a6"));
                    holder.select_contact.setPadding(15,15,15,15);
                }

            }
        });
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        //top
        public TextView userName;
        public ImageView btn_slct;

        public RelativeLayout select_contact;


        public MyViewHolder(View view) {
            super(view);

            userName = (TextView) view.findViewById(R.id.userName);
//            mRecyclerView = view.findViewById(R.id.fast_scroller_recycler);
            select_contact = view.findViewById(R.id.select_contact);
            btn_slct = view.findViewById(R.id.btn_slct);
        }
    }

    @Override
    public int getItemCount() {
        if (List_all_user == null)
            return 0;
        return List_all_user.size();
    }
}
