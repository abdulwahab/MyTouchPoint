package com.example.wahab.mytouchpoint.models;

import android.widget.TextView;

public class ContactListModel {

    private String userName;

    public ContactListModel() {
    }

    public ContactListModel(String userName) {

        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
