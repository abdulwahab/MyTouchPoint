package com.example.wahab.mytouchpoint.activites;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.wahab.mytouchpoint.R;

public class LoginScreen extends AppCompatActivity {
    public RelativeLayout fb_login;
    TextView sign_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            Window w = getWindow();
//            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
//        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        InitView();
        InitListener();

    }

    private void InitView() {
        fb_login = findViewById(R.id.fb_login);
        sign_login = findViewById(R.id.sign_login);
    }

    private void InitListener() {
//                Toast.makeText(LoginScreen.this,"ok",Toast.LENGTH_SHORT).show();

        fb_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginScreen.this, SelectContact.class);
                startActivity(intent);
            }
        });
//        sign_login.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(LoginScreen.this, SelectContact.class);
//                startActivity(intent);
//            }
//        });

    }
}
