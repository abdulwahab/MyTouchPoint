package com.example.wahab.mytouchpoint.activites;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.example.wahab.mytouchpoint.R;
import com.example.wahab.mytouchpoint.adapter.ContactListAdapter;
import com.example.wahab.mytouchpoint.models.ContactListModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;

public class SelectContact extends AppCompatActivity {
    private List<ContactListModel> list = new ArrayList<>();
    private IndexFastScrollRecyclerView recyclerView;
    //    private RecyclerView recyclerView;
    private ContactListAdapter contactAdapter;
    ImageView backarrow;
    //    IndexFastScrollRecyclerView recyclerView;
    private List<String> mDataArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_contact);
        InitView();
        InitListener();
//        initialiseData();
        InitRecyclerView();
        initialiseUI();
        prepareFragmentData();
    }

//    protected void initialiseData() {
//        ArrayList<ContactListModel> mAlphabetItems = new ArrayList<>();
//        List<String> strAlphabets = new ArrayList<>();
//        for (int i = 0; i < list.size(); i++) {
//            String name = String.valueOf(list.get(i));
//            if (name == null || name.trim().isEmpty())
//                continue;
//
//            String word = name.substring(0, 1);
//            if (!strAlphabets.contains(word)) {
//                strAlphabets.add(word);
//                mAlphabetItems.add(new ContactListModel());
//            }
//        }
//    }

    private void InitView() {
        backarrow = findViewById(R.id.backarrow);
        recyclerView = findViewById(R.id.recycler_view_contact);
    }

    private void InitListener() {
        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void InitRecyclerView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        contactAdapter = new ContactListAdapter(list);
        recyclerView.setAdapter(contactAdapter);
    }

    protected void initialiseUI() {
        recyclerView.setIndexTextSize(15);
        recyclerView.setIndexBarColor(R.color.white);
        recyclerView.setIndexBarCornerRadius(0);
        recyclerView.setIndexBarTransparentValue((float) 0.4);
        recyclerView.setIndexbarMargin(0);
        recyclerView.setIndexbarWidth(40);
        recyclerView.setPreviewPadding(0);
        recyclerView.setIndexBarTextColor(R.color.text_yellow);

//        recyclerView.setPreviewTextSize(60);
//        recyclerView.setPreviewColor("#33334c");
//        recyclerView.setPreviewTextColor("#FFFFFF");
//        recyclerView.setPreviewTransparentValue(0.6f);

        recyclerView.setIndexBarVisibility(true);
        recyclerView.setIndexbarHighLateTextColor("#33334c");
        recyclerView.setIndexBarHighLateTextVisibility(true);
    }

    private void prepareFragmentData() {

        ContactListModel list_data = new ContactListModel("Abdul wahab");
        list.add(list_data);

        list_data = new ContactListModel("Earl Atkins");
        list.add(list_data);

        list_data = new ContactListModel("AbuBakar Butt");
        list.add(list_data);

        list_data = new ContactListModel("Leonard Perez");
        list.add(list_data);

        list_data = new ContactListModel("Butt");
        list.add(list_data);

        list_data = new ContactListModel("Mona Liza");
        list.add(list_data);

        list_data = new ContactListModel("Earl");
        list.add(list_data);

        list_data = new ContactListModel("AbuBakar");
        list.add(list_data);

        list_data = new ContactListModel("Leonard");
        list.add(list_data);

        list_data = new ContactListModel("AbuBakar White");
        list.add(list_data);

        list_data = new ContactListModel("Mona");
        list.add(list_data);

        // soting data
        Collections.sort(list, new Comparator<ContactListModel>() {
            @Override
            public int compare(ContactListModel item, ContactListModel t1) {
                String s1 = item.getUserName();
                String s2 = t1.getUserName();
                return s1.compareToIgnoreCase(s2);
            }

        });

        contactAdapter.notifyDataSetChanged();
    }

}
